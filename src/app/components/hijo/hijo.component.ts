import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  @Input() MensajeHijo!: string;
  @Input() mensajeDos!: string;
  @Input() mensajeTres!: string;
  @Input() mensajeCuatro!: string;
  @Input() mensajeCinco!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
